﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Presentación
{
    public partial class frnIniciarSesion : Form
    {
        frnFormulario_Registro oRegistro = new frnFormulario_Registro();
        public frnIniciarSesion()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            oRegistro.Show();
            this.Hide();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Autenticacion oAuten = new Autenticacion();
            oAuten.ValidarUsuario(txtUser.Text, txtPassword.Text);
            

            PaginaPrincipal miForm = new PaginaPrincipal();
            miForm.Show();
            this.Hide();
        }
    }
}
