﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación
{
    public partial class frnFormulario_Registro : Form

    {
        public frnFormulario_Registro()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtId.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtTel.Clear();
            txtMail.Clear();
            txtUser.Clear();
            txtPassword.Clear();
            txtConfirmarPWD.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
        }
        Usuarios oUsuario = new Usuarios();
        private void button3_Click(object sender, EventArgs e)
        {
            oUsuario.IdUsuario= Int64.Parse(txtId.Text);
            oUsuario.Nombres1 = txtNombre.Text;
            oUsuario.Apellidos1 = txtApellido.Text;
            oUsuario.Telefono1 = Int64.Parse(txtTel.Text);
            oUsuario.Correo1 = txtMail.Text;
            oUsuario.Usuario1 = txtUser.Text;
            oUsuario.Contraseña1 = txtPassword.Text;
            oUsuario.ConfirmarPWD1 = txtConfirmarPWD.Text;

            oUsuario.InsertarUsuarios(Int64.Parse(txtId.Text), txtNombre.Text, txtApellido.Text, Int64.Parse(txtTel.Text), txtMail.Text, txtUser.Text, txtPassword.Text, txtConfirmarPWD.Text);
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oUsuario.IdUsuario = Int64.Parse(txtId.Text);
            oUsuario.Nombres1 = txtNombre.Text;
            oUsuario.Apellidos1 = txtApellido.Text;
            oUsuario.Telefono1 = Int64.Parse(txtTel.Text);
            oUsuario.Correo1 = txtMail.Text;
            oUsuario.Usuario1 = txtUser.Text;
            oUsuario.Contraseña1 = txtPassword.Text;
            oUsuario.ConfirmarPWD1 = txtConfirmarPWD.Text;

            oUsuario.ActualizarUsuario(Int64.Parse(txtId.Text), txtNombre.Text, txtApellido.Text, Int64.Parse(txtTel.Text), txtMail.Text, txtUser.Text, txtPassword.Text, txtConfirmarPWD.Text);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            oUsuario.IdUsuario = Int64.Parse(txtId.Text);

            oUsuario.EliminarUsuarios(Int64.Parse(txtId.Text));
        }

        private void txtId(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if ((e.KeyChar == '.') && (!txtId.Text.Contains(".")))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo se permiten números", "Validacion de número", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void txtNombre(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

                MessageBox.Show("Solo se admiten letras", "Validacion de texto", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtNombre.charactercasing = CharacterCasing.Upper;//Para MAYUSCULAS
                txtNombre.charactercasing = CharacterCasing.Lower;//Para minusculas
            }
        }
        public static bool validarEmail(string Mail)
        {
            string expresion = "\\w+()[-+.']\\w+)*@\\w+()[-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if(Regex.IsMatch(Mail, expresion, String.Empty))
            {
                if(Regex.Replace(Mail, expresion, String.Empty).Length == 0)
                { return true; }
                else
                { return false; }
            }
            else
            { return false; }
        }
        private void txtMail(object sender, KeyPressEventArgs e)
        {
            if (validarEmail(txtMail.Text))
            {

            }
            else
            {
                MessageBox.Show("Direccion de correo electronico no valida, el correo debe tener el formato: nombre@dominio.com," +
                " por favor seleccione un correo valido", "Validacion de correo electrónico", MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
                txtMail.SelectAll();
                txtMail.Focus(); 
            }
        }

        private void txtId_Key(object sender, KeyPressEventArgs e)
        {

        }
    }
}
