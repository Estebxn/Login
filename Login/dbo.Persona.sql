﻿CREATE TABLE [dbo].[Persona] (
    [Nombre]              VARCHAR (MAX)  NULL,
    [Apellido]            VARCHAR (MAX)  NULL,
    [Usuario]             NVARCHAR (MAX) NULL,
    [Contraseña]          NVARCHAR (MAX) NULL,
    [ConfirmarContraseña] NVARCHAR (MAX) NULL, 
    [Teléfono] BIGINT NULL, 
    [Correo] NVARCHAR(MAX) NULL
);

