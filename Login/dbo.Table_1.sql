﻿CREATE TABLE [dbo].[Table]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Nombre] VARCHAR(MAX) NULL, 
    [Apellido] VARCHAR(MAX) NULL, 
    [Correo] NVARCHAR(MAX) NULL, 
    [Teléfono] BIGINT NULL, 
    [Dirección] NVARCHAR(MAX) NULL, 
    [Usuario] NVARCHAR(MAX) NULL, 
    [Contraseña] NVARCHAR(MAX) NULL, 
    [Confirmar Contraseña] NVARCHAR(MAX) NULL
)
