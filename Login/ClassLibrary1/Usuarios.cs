﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Usuarios
    {
        Int64 idUsuario;
        string Nombres;
        string Apellidos;
        Int64 Telefono;
        string Correo;
        string Usuario;
        string Contraseña;
        string ConfirmarPWD;

        public long IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }

        public string Nombres1
        {
            get
            {
                return Nombres;
            }

            set
            {
                Nombres = value;
            }
        }

        public string Apellidos1
        {
            get
            {
                return Apellidos;
            }

            set
            {
                Apellidos = value;
            }
        }

        public Int64 Telefono1
        {
            get
            {
                return Telefono;
            }

            set
            {
                Telefono = value;
            }
        }

        public string Correo1
        {
            get
            {
                return Correo;
            }

            set
            {
                Correo = value;
            }
        }

        public string Usuario1
        {
            get
            {
                return Usuario;
            }

            set
            {
                Usuario = value;
            }
        }

        public string Contraseña1
        {
            get
            {
                return Contraseña;
            }

            set
            {
                Contraseña = value;
            }
        }

        public string ConfirmarPWD1
        {
            get
            {
                return ConfirmarPWD;
            }

            set
            {
                ConfirmarPWD = value;
            }
        }

        string sentencia;
        Conexion oConex = new Conexion();

        public void InsertarUsuarios(Int64 idUsuario, string Nombres, string Apellidos, Int64 Telefono, string Correo, string Usuario, string Contraseña, string ConfirmarPWD)
        {
            sentencia = "exec AgregarUsuarios "+ idUsuario +", '" + Nombres + "', '" + Apellidos + "', " + Telefono + ", '" + Correo + "', '" + Usuario + "', '" + Contraseña + "', '" + ConfirmarPWD + "'";
            oConex.ejecutar_consultas_sql(sentencia);
        }
        public void ActualizarUsuario(Int64 idUsuario, string Nombres, string Apellidos, Int64 Telefono, string Correo, string Usuario, string Contraseña, string ConfirmarPWD)
        {
            sentencia = "exec ActualizarUsuarios "+ idUsuario +", '" + Nombres + "', '" + Apellidos + "', " + Telefono + ", '" + Correo + "', '" + Usuario + "', '" + Contraseña + "', '" + ConfirmarPWD + "'";
            oConex.ejecutar_consultas_sql(sentencia);
        }
        public void EliminarUsuarios(Int64 idUsuario)
        {
            sentencia = "exec EliminarUsuarios " + idUsuario + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }
    }
}
